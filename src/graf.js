const languages = [
  ["JavaScript", 70],
  ["Java", 45],
  ["Python", 39],
  ["C#", 34],
  ["PHP", 31],
  ["C++", 25],
];

document.addEventListener("DOMContentLoaded", () => {
  const draw = document.getElementById("draw");
  const ctx = draw.getContext("2d");

  ctx.font = "20px sans-serif";
  let iwidth = 0,
    a;
  for (let i = 0; i < languages.length; ++i) {
    a = ctx.measureText(languages[i][0]).width;
    if (a > iwidth) iwidth = a;
  }
  draw.height = iwidth * languages.length + 5 * (languages.length - 1);
  ctx.font = "20px sans-serif";
  ctx.textAlign = "center";
  ctx.textBaseline = "left";
  const iheight = draw.height - 25;

  let xi;
  for (let i = 0; i < languages.length; ++i) {
    a = (iheight * languages[i][1]) / 100;
    xi = i * (iwidth + 5);
    yi = draw.height - a;
    ctx.fillRect(0, xi, a, iwidth);
  }
});
