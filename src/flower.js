document.addEventListener("DOMContentLoaded", () => {
  const draw = document.getElementById("draw");
  const ctx = draw.getContext("2d");

  ctx.translate(150, 150);
  ctx.save();
  for (let i = 0; i < 16; ++i) {
    ctx.beginPath();
    ctx.fillStyle = "red";
    ctx.moveTo(0, 0);
    ctx.quadraticCurveTo(-50, -75, 0, -150);
    ctx.quadraticCurveTo(50, -75, 0, 0);
    ctx.fill();
    ctx.rotate(Math.PI / 4);
  }

  ctx.restore();
  ctx.globalCompositeOperation = "destination-over";
  ctx.beginPath();
  ctx.arc(0, 0, 30, -Math.PI / 2, Math.PI / 2);
  ctx.fill();
  ctx.beginPath();
  ctx.arc(0, 0, 29, Math.PI / 2, 1.5 * Math.PI);
  ctx.stroke();
});
